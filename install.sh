#!/bin/bash


if [[ $1 == "server" ]]; then
	apt -y install stow vim #TODO make this distro-agnostic
	cp serverbash/.bashrc bash/.bashrc
	[[ -f ../.bashrc ]] && rm -f ../.bashrc
	[[ -f ../.inputrc ]] && rm -f ../.inputrc
	[[ -f ../.bash_profile ]] && rm -f ../.bash_profile
	[[ -f ../.profile ]] && rm -f ../.profile
	stow bash/       \
	     inputrc/    \
	     vim/
elif [[ $1 == "desktop" ]]; then
	apt -y install stow #TODO make this distro-agnostic (and also DRY)
	stow bash/       \
	     fonts/      \
	     icons/      \
	     inputrc/    \
	     kitty/      \
	     nvim/       \
	     redshift/   \
	     streamlink/ \
	     themes/     \
	     tmux/
else echo "Please specify 'server' or 'desktop'" && exit 1
fi
