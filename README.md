## My .dotfiles

This is a complete rewrite of my configuration files. The ones I'm using right now contain lots of legacy stuff from when I was very inexperienced.

This time I'll also use [GNU Stow](https://www.gnu.org/software/stow/) instead of writing my own convoluted symlinking script.
