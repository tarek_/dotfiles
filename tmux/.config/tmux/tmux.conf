#
# tmux.conf
#

# set prefix to Alt-i
set-option -g prefix M-i

# reload configuration file
bind r source-file ~/.config/tmux/tmux.conf \; display "Reloaded!"

# 256 colours
# set -g default-terminal "screen-256color"
set -g default-terminal "tmux-256color"

# colours and styling
setw -g window-status-style fg=black,bg=green
setw -g window-status-current-style fg=red,bold,bg=black
# setw -g pane-border-style fg=green,bg=black
# setw -g pane-active-border-style fg=white,bg=green
set -g message-style fg=white,bold,bg=black
set -g status-left-length 40
set -g status-left "#[fg=black]Session: #S | " 
set -g status-justify centre
setw -g monitor-activity on
set -g visual-activity on

# set delay between prefix and command
set -sg escape-time 1

# start counting at 1
set -g base-index 1
setw -g pane-base-index 1

# split panes
bind i split-window -h
bind - split-window -v

# move left and right through windows
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# resize panes
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# resize panes incrementally
bind -r h resize-pane -L 1
bind -r j resize-pane -D 1
bind -r k resize-pane -U 1
bind -r l resize-pane -R 1

# sometimes it's just easier to resize with the mouse
# (right tool for the right job)
set -g mouse on

# send input to all panes at the same time.
bind C-n setw synchronize-panes
setw -g window-status-current-format	'#{?pane_synchronized,#[bg=red],}#I:#W'
setw -g window-status-format		'#{?pane_synchronized,#[bg=red],}#I:#W'

# copy mode
setw -g mode-keys vi
bind y copy-mode

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n 'C-h' if-shell "$is_vim" 'send-keys C-h' 'select-pane -L'
bind-key -n 'C-j' if-shell "$is_vim" 'send-keys C-j' 'select-pane -D'
bind-key -n 'C-k' if-shell "$is_vim" 'send-keys C-k' 'select-pane -U'
bind-key -n 'C-l' if-shell "$is_vim" 'send-keys C-l' 'select-pane -R'
tmux_version='$(tmux -V | sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'
if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\'  'select-pane -l'"
if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\\\'  'select-pane -l'"
bind-key -n 'C-Space' if-shell "$is_vim" 'send-keys C-Space' 'select-pane -t:.+'

bind-key -T copy-mode-vi 'C-h' select-pane -L
bind-key -T copy-mode-vi 'C-j' select-pane -D
bind-key -T copy-mode-vi 'C-k' select-pane -U
bind-key -T copy-mode-vi 'C-l' select-pane -R
bind-key -T copy-mode-vi 'C-\' select-pane -l
bind-key -T copy-mode-vi 'C-Space' select-pane -t:.+
