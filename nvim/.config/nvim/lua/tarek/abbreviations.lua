-- spelling
vim.cmd('iabb teh the')
vim.cmd('iabb pirnt print')

-- other useful stuff
vim.cmd('iabb def def():<left><left><left>')
vim.cmd('iabb bsb #!/bin/bash')
vim.cmd('iabb psb #!/usr/bin/env python')
vim.cmd('iabb cbox - [ ]')

-- to be expanded
