local opts = { noremap = true, silent = true }
local km = vim.keymap.set

km("n", "<Space>", "<nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Essential bindings
km("i", "jj", "<C-]><Esc>", opts)                               -- jj to exit insert mode (the <C-]> triggers the abbrev).
km("i", "<C-j>", "<ESC>la", opts)                               -- jump over closing quote, paren, etc.
km("n", "<C-f>", "<cmd>NvimTreeToggle<cr>", opts)               -- toggle NvimTree
km("n", "<leader>+", "<C-a>", opts)                             -- increment number
km("n", "<leader>-", "<C-x>", opts)                             -- decrement number
km("n", "<M-k>", "<Esc>:m .-2<CR>", opts)                       -- move line up
km("n", "<M-j>", "<Esc>:m .+1<CR>", opts)                       -- move line down
km("n", "<leader>z", "i<C-r>=expand('%:t')<CR><Space>", opts)   -- insert current filename without path
                                                                -- TODO I should have a wrapper (put quotes etc. around things)
-- Center Screen after incremental search
km("n", "n", "nzz", opts)
km("n", "N", "Nzz", opts)
km("n", "*", "*zz", opts)
km("n", "#", "#zz", opts)

-- vim/tmux navigation
local nvim_tmux_nav = require('nvim-tmux-navigation')

nvim_tmux_nav.setup {
    disable_when_zoomed = true                                  -- defaults to false
}
km('n', "<C-h>", nvim_tmux_nav.NvimTmuxNavigateLeft)
km('n', "<C-j>", nvim_tmux_nav.NvimTmuxNavigateDown)
km('n', "<C-k>", nvim_tmux_nav.NvimTmuxNavigateUp)
km('n', "<C-l>", nvim_tmux_nav.NvimTmuxNavigateRight)
km('n', "<C-\\>", nvim_tmux_nav.NvimTmuxNavigateLastActive)
km('n', "<C-Space>", nvim_tmux_nav.NvimTmuxNavigateNext)


-- Telescope bindings
km("n", "<leader>fd", "<cmd>Telescope find_files<CR>", opts)
km("n", "<leader>lg", "<cmd>Telescope live_grep<CR>", opts)
km("n", "<leader>gc", "<cmd>Telescope git_commits<CR>", opts)
km("n", "<leader>gs", "<cmd>Telescope git_status<CR>", opts)
