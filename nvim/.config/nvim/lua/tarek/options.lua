local options = {
    guicursor = "",
    termguicolors = true,
    background = "dark",
    fileencoding = "utf-8",
    hlsearch = false,
    incsearch = true,
    inccommand = "split",
    smartcase = true,
    ignorecase = true,
    number = true,
    relativenumber = true,
    tabstop = 4,
    softtabstop = 4,
    shiftwidth = 4,
    expandtab = true,
    smartindent = true,
    signcolumn = "yes",
    splitright = true,
    splitbelow = true,
    wrap = false,
    scrolloff = 999,
    clipboard = "unnamedplus",
    virtualedit = "block",
}

for key, value in pairs(options) do
    vim.opt[key] = value
end

-- define leader key
vim.g.mapleader = " "
